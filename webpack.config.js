// "Constants" used to easily customize your app
var APP_TITLE = "Reactive"; // what's inside index.html <title> tag

// Use NodeJS "path" class to create more robust paths
var path = require('path'),
    // Build directory will be "./build"
    buildPath = path.join(__dirname, "build"),
    // Source directory will be "./src"
    srcPath = path.join(__dirname, "src"),
    // Cache directory to speed up builds
    cachePath = path.join(__dirname, "webpack-cache"),
    
    // Use a plugin (must be npm installed) to auto-generate index.html
    // We also use `html-webpack-template` to bootstrap a nicer HTML template
    HtmlWebpackPlugin = require('html-webpack-plugin');
    
    
module.exports = {
    // Change this if you're building source and build into a subdirectory
    context: __dirname,
    
    // Currently configured for a single entry point, though multiples are possible
    entry: path.join(srcPath, "main.js"),
    
    output: {
        path: buildPath,
        filename: "bundle.js"
    },
    
    module : {
        // Loaders recognize file extensions (the "test" property)
        // and run the appropriate transpiler.
        // Loaders must be installed manually (eg: npm install --save-dev babel-loader)
        loaders : [
            // Run .js files through the ES2015 transpiler for ES-next (ES6) syntax
            {
                test : /\.js$/,
                loader : 'babel-loader',
                include : srcPath,
                query : {
                     presets: ['es2015']
                }
            },
            
            // Run .jsx files through ES2015 AND React transpilers
            {
                test : /\.jsx$/,
                loader : 'babel-loader',
                query : {
                    presets: ['react', 'es2015']
                }
            }
            
        ]
    },
    
    plugins: [
        // Auto-generate HTML index.html file
        new HtmlWebpackPlugin({
            //template: path.resolve('node_modules/html-webpack-template/index.html'),
            title : APP_TITLE,
            //mobile : true,
            //appMountId : "main",
            showErrors : true
        })
    ]
};