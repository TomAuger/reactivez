# Setting up a ReactJS Site on Codeanywhere

1. Set up a GitHub repository, or a Bitbucket repo, and create a README.md file so you can test that the GitHub sync is working in Codeanywhere.
1. Make sure you have not only authorized Codeanywhere to use GitHub, but you have also copied the Default RSA key from Codeanywhere into your GitHub Public Keys section in your Account settings
1. Now spin up a new server, choosing your GitHub or Bitbucket repo. You can select HTML5 or a node.js server. As long as it has `npm` installed.
1. Open up your ssh window in CA
2. `npm init` to create the `package.json` config file. You can use all the defaults, but at least enter the author!
3. `npm install --S react react-dom` This installs react and react-dom and **S**aves them
4. `npm install --save-dev webpack webpack-dev-server` This installs **Webpack**, which we'll be using to minify/combineour code. `--save-dev` tells `npm` to add this as a development-only dependency. I'm taking a risk here, because the tutorial I'm following actually tells me to install the webpack dev server using the `-g` global flag, rather than installing it to the local repo. We'll see what happens.
5. `npm install --save-dev babel-loader babel-core` This installs [Babel](https://babeljs.io/docs/setup/#webpack), which transpiles ECMA2015 and JSX, both of which we'll need for React.
6. `npm install --save-dev babel-preset-es2015 babel-preset-react` Apparently, after Babel v6.x, it doesn't ship with any transformations enabled, so these two [presets](http://babeljs.io/docs/plugins/#presets) turn on a bunch of transformation "plugins" to transpile most of the common ECMAScript *next* language elements as well as most of the React JSX markup.
7. Oh, you may want to let Codeanywhere know that `.jsx` files are JavaScript (until CodeMirror implements JSX properly I guess) by editing the User "languages" Preference and overriding the entry for "JavaScript" to include "jsx" in the list of extensions.
9. `npm install --save-dev sass-loader css-loader style-loader` To get SASS and CSS working.
8. `npm install --save-dev react-hot-loader` This installs the hot-loader that preserves state whenever the watcher detects a file change when you're running `webpack-dev-server`
9. `npm install --save-dev html-webpack-plugin html-webpack-template` The HTMl plugin will actually generate our HTML file for us, since we're creating pure React apps that have no other HTMl dependencies. If we don't need that (React is just a component of the site) then we can skip this.

## Configuring Webpack

This is where the magic lies - all the configuration for the loaders, the plugins, the input and out folders. It all depends on how you want to set up your directory structure.

I'm going to recommend the following directory structure:

```
[] root
|
+-- build
|	+-- index.html
|	+-- build.js
|
+-- src
	+--main.js
	|
	+-- views
		+-- SomeComponent.jsx
		|
		+-- styles
		+-- SomeComponent.scss
``` 

At least, that's the plan.

1. Create `webpack-config.js` in your root folder (where your `package.json` file is)
2. 