import React from 'react';
import ReactDOM from 'react-dom';

import Main from './view/Main.jsx';

// Hopefully we can skip this line if we get webpack-html-template working...
document.body.innerHTML = '<div id="main"></div>';

// Inject our Main.jsx into the "main" DIV, and off we go!
ReactDOM.render(React.createElement(Main), document.getElementById("main"));